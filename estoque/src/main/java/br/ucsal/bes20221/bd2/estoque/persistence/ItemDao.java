package br.ucsal.bes20221.bd2.estoque.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.ucsal.bes20221.bd2.estoque.domain.Item;

public class ItemDao extends Dao<Item> {

	private static ItemDao instance;

	public static ItemDao getInstance() {
		if (instance == null) {
			instance = new ItemDao();
		}
		return instance;
	}

	private ItemDao() {
	}

	@Override
	protected String getQuerySelect() {
		return "select id as i_id, nome as i_nome from item";
	}

	@Override
	protected String getQueryInsert() {
		return "insert into item (nome) values (?)";
	}

	@Override
	protected String getQueryUpdate() {
		return "update item set nome = ? where id=?";
	}

	@Override
	protected String getQueryDelete() {
		return "delete from item where id=?";
	}

	@Override
	protected void setParametersInsert(Item object, PreparedStatement stmt) throws SQLException {
		stmt.setString(1, object.getNome());
	}

	@Override
	protected void setParametersUpdate(Item object, PreparedStatement stmt) throws SQLException {
		setParametersInsert(object, stmt);
		stmt.setInt(2, object.getId());
	}

	@Override
	protected void setParametersDelete(Item object, PreparedStatement stmt) throws SQLException {
		stmt.setInt(1, object.getId());
	}

	@Override
	protected void setIdObject(Item object, PreparedStatement stmt) throws SQLException {
		object.setId(getGeneratedId(stmt));
	}

	@Override
	protected Item rs2Object(ResultSet rs) throws SQLException {
		return new Item(rs.getInt("i_id"), rs.getString("i_nome"));
	}

}
