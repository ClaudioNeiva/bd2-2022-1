package br.ucsal.bes20221.bd2.estoque.exception;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ApplicationException(String message) {
		super(message);
	}

}
