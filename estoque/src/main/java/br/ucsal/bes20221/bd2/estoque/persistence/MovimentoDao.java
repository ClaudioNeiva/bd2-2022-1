package br.ucsal.bes20221.bd2.estoque.persistence;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.ucsal.bes20221.bd2.estoque.domain.Item;
import br.ucsal.bes20221.bd2.estoque.domain.Movimento;

public class MovimentoDao extends Dao<Movimento> {

	private static MovimentoDao instance;

	public static MovimentoDao getInstance() {
		if (instance == null) {
			instance = new MovimentoDao();
		}
		return instance;
	}

	private MovimentoDao() {
	}

	@Override
	protected String getQuerySelect() {
		return "select m.id as m_id, m.data, m.qtd, m.historico, i.id as i_id, i.nome as i_nome from movimento m inner join item i on (i.id = m.id_item)";
	}

	@Override
	protected String getQueryInsert() {
		return "insert into movimento (data, id_item, qtd, historico) values (?, ?, ?, ?)";
	}

	@Override
	protected String getQueryUpdate() {
		return "update movimento set data = ?, id_item = ?, qtd = ?, historico = ? where id = ?";
	}

	@Override
	protected String getQueryDelete() {
		return "delete from movimento where id = ?";
	}

	@Override
	protected void setParametersInsert(Movimento object, PreparedStatement stmt) throws SQLException {
		stmt.setDate(1, Date.valueOf(object.getData()));
		stmt.setInt(2, object.getItem().getId());
		stmt.setInt(3, object.getQtd());
		stmt.setString(4, object.getHistorico());
	}

	@Override
	protected void setParametersUpdate(Movimento object, PreparedStatement stmt) throws SQLException {
		setParametersInsert(object, stmt);
		stmt.setInt(5, object.getId());
	}

	@Override
	protected void setParametersDelete(Movimento object, PreparedStatement stmt) throws SQLException {
		stmt.setInt(1, object.getId());
	}

	@Override
	protected void setIdObject(Movimento object, PreparedStatement stmt) throws SQLException {
		object.setId(getGeneratedId(stmt));
	}

	@Override
	protected Movimento rs2Object(ResultSet rs) throws SQLException {
		Item item = ItemDao.getInstance().rs2Object(rs);
		return new Movimento(rs.getInt("m_id"), rs.getDate("data").toLocalDate(), item, rs.getInt("qtd"), rs.getString("historico"));
	}

}
