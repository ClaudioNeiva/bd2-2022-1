package br.ucsal.bes20221.bd2.estoque.domain;

import java.util.Objects;

public class Item {

	private Integer id;

	private String nome;

	public Item(String nome) {
		super();
		this.nome = nome;
	}

	public Item(Integer id, String nome) {
		this(nome);
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nome);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		return Objects.equals(nome, other.nome);
	}

	@Override
	public String toString() {
		return "Item [nome=" + nome + "]";
	}

}
