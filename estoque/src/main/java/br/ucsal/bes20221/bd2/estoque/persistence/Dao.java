package br.ucsal.bes20221.bd2.estoque.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//TODO Criar um cache para tratar referências aos mesmos objetos. Por exemplo, mesmo item apontado por diversos movimentos. 
public abstract class Dao<T> {

	protected abstract String getQuerySelect();

	protected abstract String getQueryInsert();

	protected abstract String getQueryUpdate();

	protected abstract String getQueryDelete();

	protected abstract void setParametersInsert(T object, PreparedStatement stmt) throws SQLException;

	protected abstract void setParametersUpdate(T object, PreparedStatement stmt) throws SQLException;

	protected abstract void setParametersDelete(T object, PreparedStatement stmt) throws SQLException;

	protected abstract void setIdObject(T object, PreparedStatement stmt) throws SQLException;
	
	protected abstract T rs2Object(ResultSet rs) throws SQLException;

	public List<T> findAll() throws SQLException {
		try (Statement stmt = DbUtil.getConnection().createStatement();
				ResultSet rs = stmt.executeQuery(getQuerySelect())) {
			return rs2Objects(rs);
		}
	}

	public void insert(T object) throws SQLException {
		try (PreparedStatement stmt = DbUtil.getConnection().prepareStatement(getQueryInsert(),
				Statement.RETURN_GENERATED_KEYS)) {
			setParametersInsert(object, stmt);
			stmt.executeUpdate();
			setIdObject(object, stmt);
		}
	}

	public void update(T object) throws SQLException {
		try (PreparedStatement stmt = DbUtil.getConnection().prepareStatement(getQueryUpdate())) {
			setParametersUpdate(object, stmt);
			stmt.executeUpdate();
		}
	}

	public void delete(T object) throws SQLException {
		try (PreparedStatement stmt = DbUtil.getConnection().prepareStatement(getQueryDelete())) {
			setParametersDelete(object, stmt);
			stmt.executeUpdate();
		}
	}

	protected static Integer getGeneratedId(Statement stmt) throws SQLException {
		try (ResultSet idRs = stmt.getGeneratedKeys();) {
			idRs.next();
			return idRs.getInt(1);
		}
	}

	private List<T> rs2Objects(ResultSet rs) throws SQLException {
		List<T> itens = new ArrayList<>();
		while (rs.next()) {
			itens.add(rs2Object(rs));
		}
		return itens;
	}

}
