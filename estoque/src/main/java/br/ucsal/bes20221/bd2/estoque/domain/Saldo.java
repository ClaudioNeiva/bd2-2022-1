package br.ucsal.bes20221.bd2.estoque.domain;

import java.time.LocalDate;
import java.util.Objects;

public class Saldo {

	private LocalDate data;

	private Item item;

	private Integer qtd;

	public Saldo(LocalDate data, Item item, Integer qtd) {
		super();
		this.data = data;
		this.item = item;
		this.qtd = qtd;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Integer getQtd() {
		return qtd;
	}

	public void setQtd(Integer qtd) {
		this.qtd = qtd;
	}

	@Override
	public int hashCode() {
		return Objects.hash(data, item, qtd);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Saldo other = (Saldo) obj;
		return Objects.equals(data, other.data) && Objects.equals(item, other.item) && Objects.equals(qtd, other.qtd);
	}

	@Override
	public String toString() {
		return "Saldo [data=" + data + ", item=" + item + ", qtd=" + qtd + "]";
	}

}
