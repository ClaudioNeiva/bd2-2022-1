package br.ucsal.bes20221.bd2.estoque.domain;

import java.time.LocalDate;
import java.util.Objects;

public class Movimento {

	private Integer id;

	private LocalDate data;

	private Item item;

	private Integer qtd;

	private String historico;

	public Movimento(LocalDate data, Item item, Integer qtd, String historico) {
		super();
		this.data = data;
		this.item = item;
		this.qtd = qtd;
		this.historico = historico;
	}

	public Movimento(Integer id, LocalDate data, Item item, Integer qtd, String historico) {
		this(data, item, qtd, historico);
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Integer getQtd() {
		return qtd;
	}

	public void setQtd(Integer qtd) {
		this.qtd = qtd;
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	@Override
	public int hashCode() {
		return Objects.hash(data, historico, id, item, qtd);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movimento other = (Movimento) obj;
		return Objects.equals(data, other.data) && Objects.equals(historico, other.historico)
				&& Objects.equals(id, other.id) && Objects.equals(item, other.item) && Objects.equals(qtd, other.qtd);
	}

	@Override
	public String toString() {
		return "Movimento [data=" + data + ", item=" + item + ", qtd=" + qtd + ", historico=" + historico + "]";
	}

}
