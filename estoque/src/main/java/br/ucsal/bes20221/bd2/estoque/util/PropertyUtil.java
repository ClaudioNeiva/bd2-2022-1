package br.ucsal.bes20221.bd2.estoque.util;

import java.io.IOException;
import java.util.Properties;

public class PropertyUtil {

	private static Properties properties = new Properties();

	static {
		try {
			properties.load(PropertyUtil.class.getClassLoader().getResourceAsStream("application.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private PropertyUtil() {
	}

	public static String get(String key) {
		return properties.getProperty(key);
	}

}
