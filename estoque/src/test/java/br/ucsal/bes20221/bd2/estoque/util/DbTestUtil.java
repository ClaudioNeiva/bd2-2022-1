package br.ucsal.bes20221.bd2.estoque.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import br.ucsal.bes20221.bd2.estoque.persistence.DbUtil;

public class DbTestUtil {

	private DbTestUtil() {
	}

	public static void deleteTables(String... tableNames) throws SQLException {
		for (String tableName : tableNames) {
			deleteAll(tableName);
		}
	}

	public static void deleteAll(String tableName) throws SQLException {
		String query = "delete from " + tableName;
		try (PreparedStatement stmt = DbUtil.getConnection().prepareStatement(query)) {
			stmt.executeUpdate();
		}
	}

}
