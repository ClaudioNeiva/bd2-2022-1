package br.ucsal.bes20221.bd2.estoque.persistence;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import br.ucsal.bes20221.bd2.estoque.domain.Item;
import br.ucsal.bes20221.bd2.estoque.util.DbTestUtil;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ItemDaoTest {

	private ItemDao itemDao = ItemDao.getInstance();

	@BeforeAll
	void setupAll() throws SQLException {
		DbTestUtil.deleteTables("movimento", "saldo");
	}

	@BeforeEach
	void setup() throws SQLException {
		DbTestUtil.deleteTables("item");
	}

	@Test
	void testarInsert() throws SQLException {
		Item item1 = new Item("parafuso");
		itemDao.insert(item1);

		List<Item> itensEsperados = Arrays.asList(item1);
		verificarItens(itensEsperados);
	}

	@Test
	void testarDelete() throws SQLException {
		Item item1 = new Item("parafuso");
		Item item2 = new Item("porca");
		itemDao.insert(item1);
		itemDao.insert(item2);
		itemDao.delete(item1);

		List<Item> itensEsperados = Arrays.asList(item2);
		verificarItens(itensEsperados);
	}

	@Test
	void testarUpdate() throws SQLException {
		Item item1 = new Item("parafuso");
		Item item2 = new Item("porca");
		itemDao.insert(item1);
		itemDao.insert(item2);
		item2.setNome("rebite");
		itemDao.update(item2);

		List<Item> itensEsperados = Arrays.asList(item1, item2);
		verificarItens(itensEsperados);
	}

	private void verificarItens(List<Item> itensEsperados) throws SQLException {
		List<Item> itensAtuais = itemDao.findAll();
		Assertions.assertThat(itensEsperados).hasSize(itensAtuais.size()).hasSameElementsAs(itensAtuais);
	}

}
