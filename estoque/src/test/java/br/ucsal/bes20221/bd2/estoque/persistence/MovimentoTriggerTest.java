package br.ucsal.bes20221.bd2.estoque.persistence;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import br.ucsal.bes20221.bd2.estoque.domain.Item;
import br.ucsal.bes20221.bd2.estoque.domain.Movimento;
import br.ucsal.bes20221.bd2.estoque.domain.Saldo;
import br.ucsal.bes20221.bd2.estoque.util.DbTestUtil;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MovimentoTriggerTest {

	private ItemDao itemDao = ItemDao.getInstance();
	private MovimentoDao movimentoDao = MovimentoDao.getInstance();
	private SaldoDao saldoDao = SaldoDao.getInstance();

	private Item item1;
	private Item item2;

	@BeforeAll
	void setupAll() throws SQLException {
		DbTestUtil.deleteTables("movimento", "saldo", "item");
		item1 = new Item("parafuso");
		item2 = new Item("porca");
		itemDao.insert(item1);
		itemDao.insert(item2);
	}

	@BeforeEach
	void setup() throws SQLException {
		DbTestUtil.deleteTables("movimento", "saldo");
	}

	@Test
	@DisplayName("Primeiro movimento.")
	void testarInsert1() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		inserirMovimento(data1, item1, 5, "Compra de parafusos.");

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 5));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Dois movimentos para o mesmo item, segundo movimento inserido em data posterior.")
	void testarInsert2() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		LocalDate data2 = LocalDate.of(2022, 4, 3);
		inserirMovimento(data2, item1, 10, "Mais compra de parafusos.");

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 5), new Saldo(data2, item1, 15));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Dois movimentos para o mesmo item, segundo movimento inserido em data anterior.")
	void testarInsert3() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		LocalDate data2 = LocalDate.of(2022, 3, 13);
		inserirMovimento(data2, item1, 10, "Mais compra de parafusos.");

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 15), new Saldo(data2, item1, 10));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Três movimentos para o mesmo item, terceiro movimento inserido entre os dois existentes.")
	void testarInsert4() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		LocalDate data2 = LocalDate.of(2022, 4, 3);
		inserirMovimento(data2, item1, 10, "Mais compra de parafusos.");
		LocalDate data3 = LocalDate.of(2022, 4, 1);
		inserirMovimento(data3, item1, 2, "Mais compra de parafusos.");

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 5), new Saldo(data3, item1, 7),
				new Saldo(data2, item1, 17));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Três movimentos para o mesmo item, terceiro movimento inserido antes dos dois existentes.")
	void testarInsert5() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		LocalDate data2 = LocalDate.of(2022, 4, 3);
		inserirMovimento(data2, item1, 10, "Mais compra de parafusos.");
		LocalDate data3 = LocalDate.of(2022, 2, 1);
		inserirMovimento(data3, item1, 2, "Mais compra de parafusos.");

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data3, item1, 2), new Saldo(data1, item1, 7),
				new Saldo(data2, item1, 17));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Remoção do único movimento existente.")
	void testarDelete1() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		Movimento movimento = inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		movimentoDao.delete(movimento);

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 0));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Remoção de um movimento com 2 movimentos posteriores.")
	void testarDelete2() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		Movimento movimento1 = inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		LocalDate data2 = LocalDate.of(2022, 4, 3);
		inserirMovimento(data2, item1, 10, "Mais compra de parafusos.");
		LocalDate data3 = LocalDate.of(2022, 5, 1);
		inserirMovimento(data3, item1, 2, "Mais compra de parafusos.");
		movimentoDao.delete(movimento1);

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 0), new Saldo(data2, item1, 10),
				new Saldo(data3, item1, 12));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Remoção de um movimento entre 2 outros movimentos.")
	void testarDelete3() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		LocalDate data2 = LocalDate.of(2022, 4, 3);
		Movimento movimento2 = inserirMovimento(data2, item1, 10, "Mais compra de parafusos.");
		LocalDate data3 = LocalDate.of(2022, 5, 1);
		inserirMovimento(data3, item1, 2, "Mais compra de parafusos.");
		movimentoDao.delete(movimento2);

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 5), new Saldo(data2, item1, 5),
				new Saldo(data3, item1, 7));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Remoção de um movimento após 2 outros movimentos.")
	void testarDelete4() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		LocalDate data2 = LocalDate.of(2022, 4, 3);
		inserirMovimento(data2, item1, 10, "Mais compra de parafusos.");
		LocalDate data3 = LocalDate.of(2022, 5, 1);
		Movimento movimento3 = inserirMovimento(data3, item1, 2, "Mais compra de parafusos.");
		movimentoDao.delete(movimento3);

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 5), new Saldo(data2, item1, 15),
				new Saldo(data3, item1, 15));
		verificarSaldos(saldosEsperados);
	}

	@Test
	@DisplayName("Atualização do único movimento existente.")
	void testarUpdate1() throws SQLException {
		LocalDate data1 = LocalDate.of(2022, 3, 28);
		Movimento movimento = inserirMovimento(data1, item1, 5, "Compra de parafusos.");
		movimento.setQtd(12);
		movimentoDao.update(movimento);

		List<Saldo> saldosEsperados = Arrays.asList(new Saldo(data1, item1, 12));
		verificarSaldos(saldosEsperados);
	}
	
	// TODO Criar testes para mais cenários de atualização.
	
	private void verificarSaldos(List<Saldo> saldosEsperados) throws SQLException {
		List<Saldo> saldosAtuais = saldoDao.findAll();
		Assertions.assertThat(saldosEsperados).hasSize(saldosAtuais.size()).hasSameElementsAs(saldosAtuais);
	}

	private Movimento inserirMovimento(LocalDate data1, Item item, Integer qtd, String historico) throws SQLException {
		Movimento movimento = new Movimento(data1, item, qtd, historico);
		movimentoDao.insert(movimento);
		return movimento;
	}

}
