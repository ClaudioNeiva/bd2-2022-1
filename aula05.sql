-- Criação das tabelas

create table veiculo (
	id					serial			not null,		
	placa				char(7)			not null,    	
	ano_fabricacao		integer			not null, 
	data_aquisicao		date 			not null, 
	data_venda			date			    null, 	
	quilometragem		integer			not null, 
	codigo_grupo		smallint		not null,		
	constraint pk_veiculo
		primary key (id),
	constraint un_veiculo_placa
		unique (placa),
	constraint ck_veiculo_quilometragem
		check (quilometragem >= 0));	

create table grupo (
	codigo				smallserial		not null, 		
	nome				varchar(40)		not null,
	constraint pk_grupo
		primary key (codigo));

create table acessorio (
    sigla				varchar(10)		not null,
    nome				varchar(40)		not null,
    constraint pk_acessorio
    	primary key (sigla));
        
create table veiculo_acessorio (
    id_veiculo			integer			not null,
    sigla_acessorio		varchar(10)		not null,
    constraint pk_veiculo_acessorio
    	primary key (id_veiculo, sigla_acessorio));
    
-- Criação das chaves estrangeiras

alter table veiculo
	add constraint fk_veiculo_grupo
		foreign key (codigo_grupo)
		references grupo;

alter table veiculo_acessorio 
	add constraint fk_veiculo_acessorio_veiculo
    	foreign key (id_veiculo)
        references veiculo
        on delete cascade,
	add constraint fk_veiculo_acessorio_acessorio
    	foreign key (sigla_acessorio)
        references acessorio
        on update cascade;


-- 

delete 
from veiculo;

delete 
from grupo;

insert into grupo (nome)
values
('básico'),
('luxo');

insert into veiculo (placa, ano_fabricacao, data_aquisicao, data_venda, quilometragem, codigo_grupo)
values
('FGH4567', 2015, '2016-01-03', null, 123700, (select codigo from grupo where nome = 'básico')),
('XYZ5678', 2015, '2015-02-05', '2017-08-03', 456700, (select codigo from grupo where nome = 'luxo')),
('ABC1234', 2019, '2022-01-05', null, 200, (select codigo from grupo where nome = 'luxo'));

select *
from grupo;

select *
from veiculo;

update 	veiculo
	set ano_fabricacao = 2020,
    	data_aquisicao = '2022-01-07'
where	placa = 'ABC1234';        

select 'Claudio '||trim('Neiva        ')||'*',
		length (concat('Claudio ','Neiva')),
        position('Neiva' in 'Claudio Neiva') ;

select * 
from veiculo
where placa <> 'asd';

select * 
from veiculo
where data_venda is not null;

select	placa,
		data_aquisicao,
		coalesce(data_venda :: varchar, 'Veículo não vendido') as data_venda,
		coalesce(cast(data_venda as varchar), 'Veículo não vendido') as data_venda
from veiculo
where upper(placa) like 'ABC%';

select  ano_fabricacao
from	veiculo;

select  distinct ano_fabricacao
from	veiculo;

select CURRENT_DATE, CURRENT_TIME, CURRENT_TIMESTAMP;

select extract (year from CURRENT_DATE);
-- year, month, day, hour, minute...
select extract (hour from CURRENT_TIMESTAMP);
-- year, month, day, hour, minute...

select  count(*) as qtd_total_veiculos, 
        count(data_venda) as qtd_veiculos_vendidos,
		max(quilometragem) as maior_quilometragem_rodada_um_veiculo, 
        min(quilometragem) as menor_quilometragem_rodada_um_veiculo, 
        sum(quilometragem) as soma_quilometragens, 
        avg(quilometragem) as media_quilometragens
from veiculo;

select	ano_fabricacao,
		count(*)
from	veiculo
group by ano_fabricacao;   

-- Retorne os anos de fabricacao que tiveram 2 ou mais veículos cadastrados
select	ano_fabricacao
from	veiculo
group by ano_fabricacao
having 	count(*) >= 2;   

select	placa, ano_fabricacao, codigo_grupo
from veiculo
order by codigo_grupo desc,
 ano_fabricacao desc;


-- Senha do Postgres:      postgresql

-- Dados de referência:
-- grupo: codigo
insert into grupo (nome)
values
('básico'),
('luxo');

insert into grupo (nome)
values
('Utilitário');

-- veiculo: id
insert into veiculo (placa, ano_fabricacao, data_aquisicao, data_venda, quilometragem, codigo_grupo)
values
('FGH4567', 2015, '2016-01-03', null, 123700, (select codigo from grupo where nome = 'básico')),
('XYZ5678', 2015, '2015-02-05', '2017-08-03', 456700, (select codigo from grupo where nome = 'luxo')),
('ABC1234', 2019, '2022-01-05', null, 200, (select codigo from grupo where nome = 'luxo'));

-- Retorno: placa, ano de fabricação e nome do grupo
-- Filtro: (nenhum)
select	v.placa,
		v.ano_fabricacao,
        g.nome
from	veiculo v 
inner join grupo g  on (g.codigo = v.codigo_grupo);

-- Retorno: placa, ano de fabricação e nome do grupo
-- Para os grupos sem veículo cadastrado, retornar como placa a mensagem: '(nenhum veículo cadastrado)'
-- Filtro: (nenhum)
insert into grupo (nome)
values
('Utilitário');

select	coalesce(v.placa,  '(nenhum veículo cadastrado)') as placa,
		v.ano_fabricacao ano,
        g.nome
from	veiculo v       
right outer join grupo g on (g.codigo = v.codigo_grupo);

select	coalesce(v.placa,  '(nenhum veículo cadastrado)') as placa,
		coalesce(cast(v.ano_fabricacao as varchar), '(nenhum veículo cadastrado)'),
        g.nome
from	grupo g         
left outer join veiculo v on (v.codigo_grupo = g.codigo);

-- Retornar: placa do veículo e sigla do acessório 
-- Junção: todos os veículos, mesmo que não tenha acessório e todos os acessórios, mesmo que não estejam instalados em veículos
-- Sigla do acessório para veículos sem acessório instalado: '(Nenhum acessório instalado)'
-- Placa de veículo para acessório não instalado em nenhum veículo: '(Acessório não instalado em nenhum veículo)'

insert into acessorio (sigla,nome)
values 
('ar','ar-condicionado'),
('dh','direção hidráulica'),
('ve','vidro elétrico'),
('te','trava elétrica');
        
insert into veiculo_acessorio (id_veiculo, sigla_acessorio)
values
((select id from veiculo where placa = 'FGH4567'),'dh'),
((select id from veiculo where placa = 'FGH4567'),'ve'),
((select id from veiculo where placa = 'FGH4567'),'te'),
((select id from veiculo where placa = 'ABC1234'),'ve');

select  coalesce(v.placa, '(Acessório não instalado em nenhum veículo)') as placa,
		coalesce(a.sigla,'(Nenhum acessório instalado)') as sigla_acessorio
from 	veiculo v
full outer join veiculo_acessorio va on (va.id_veiculo = v.id)
full outer join acessorio a on (a.sigla = va.sigla_acessorio);

-- Retonar: placa e sigla de acessório
-- Junção: combinar todas as placas com todas as sigla acessorio
select	v.placa,
		a.sigla
from	veiculo v
cross join acessorio a;


-- Natural join
alter table veiculo rename id to id_veiculo;

select * from veiculo;
select * from veiculo_acessorio;

select * 
from veiculo v 
natural join veiculo_acessorio va;

select * 
from veiculo v 
inner join veiculo_acessorio va on (va.id_veiculo = v.id_veiculo);

alter table veiculo rename id_veiculo to id;

-- Retorne: nome e telefone de todos os empregados e dependentes.
-- Para uma agenda telefônica. Devem ser retornadas apenas 2 colunas, uma para o nome e outra para o telefone.
-- Resultado esperado para os inserts abaixo:
'João'		,'7199887766'
'Maria'		,'7198899000'
'Pedro'		,'7512312445'
'Ana'		,'7134635655'
'Claudio'	,'7187873483'
'Antonio'	,'7556876888'

drop table if exists empregado cascade;
drop table if exists dependente cascade;

create table empregado (
    matricula		serial			not null,
    nome			varchar(40)		not null,
    telefone		char(11)		not null,
    constraint pk_empregado
		primary key (matricula));

create table dependente (
    matricula_empregado	integer			not null,
    seq					smallint		not null,
    nome				varchar(40)		not null,
    telefone			char(11)		not null,
    constraint pk_dependente
		primary key (matricula_empregado, seq),
	constraint fk_dependente_empregado
    	foreign key (matricula_empregado)
    references empregado);

insert into empregado (nome, telefone)
values 
('João'		,'7199887766'),
('Maria'	,'7198899000'),
('Pedro'	,'7512312445'),
('Ana'		,'7134635655');

insert into dependente (matricula_empregado, seq, nome, telefone)
values 
((select matricula from empregado where nome = 'Maria'),1,'Claudio'	,'7187873483'),
((select matricula from empregado where nome = 'Pedro'),1,'Antonio'	,'7556876888');

select * from empregado;
select * from dependente;

select 	e.nome,
		e.telefone
from 	empregado e
union
select 	d.nome,
		d.telefone
from 	dependente d;

insert into dependente (matricula_empregado, seq, nome, telefone)
values 
((select matricula from empregado where nome = 'Maria'),2,'Ana'	,'7134635655')

select 	e.nome,
		e.telefone
from 	empregado e
union all
select 	d.nome,
		d.telefone
from 	dependente d;

-- Retonar: nome dos empregados que possuam dependentes.

select	e.nome
from	empregado e
where 	e.matricula in (select d.matricula_empregado from dependente d);

-- Retonar: nome dos empregados que NÃO possuam dependentes.

select	e.nome
from	empregado e
where 	e.matricula not in (select d.matricula_empregado from dependente d);

