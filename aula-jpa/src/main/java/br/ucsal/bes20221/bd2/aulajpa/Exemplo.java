package br.ucsal.bes20221.bd2.aulajpa;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");
		EntityManager em = emf.createEntityManager();

		Aluno aluno = new Aluno(10, "Claudio"); // new

		em.getTransaction().begin();
		em.persist(aluno); // new -> managed
		// em.getTransaction().rollback();
		// em.flush();
		em.getTransaction().commit();

		em.detach(aluno);

		aluno = em.merge(aluno);

		em.getTransaction().begin();
		aluno.setNome("Maria");
		em.getTransaction().commit();

		em.getTransaction().begin();
		em.remove(aluno);
		em.getTransaction().commit();

		System.out.println("em.contains(aluno)=" + em.contains(aluno));

		emf.close();
	}
}