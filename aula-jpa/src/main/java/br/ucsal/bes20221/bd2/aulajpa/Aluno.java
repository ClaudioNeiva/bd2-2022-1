package br.ucsal.bes20221.bd2.aulajpa;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

@Entity
public class Aluno {

	@Id
	private Integer matricula;

	private String nome;

	public Aluno() {
	}

	public Aluno(Integer matricula, String nome) {
		super();
		this.matricula = matricula;
		this.nome = nome;
	}

	@PrePersist
	public void prePersist() {
		System.out.println("ativadades executadas antes da persistência....");
	}

	@PostPersist
	public void postPersist() {
		System.out.println("ativadades executadas depois da persistência....");
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		return Objects.hash(matricula, nome);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		return Objects.equals(matricula, other.matricula) && Objects.equals(nome, other.nome);
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + "]";
	}

}
