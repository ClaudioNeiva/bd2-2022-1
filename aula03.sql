-- Criação das tabelas

create table veiculo (
	id					serial			not null,		
	placa				char(7)			not null,    	
	ano_fabricacao		integer			not null, 
	data_aquisicao		date 			not null, 
	data_venda			date			    null, 	
	quilometragem		integer			not null, 
	codigo_grupo		smallint		not null,		
	constraint pk_veiculo
		primary key (id),
	constraint un_veiculo_placa
		unique (placa),
	constraint ck_veiculo_quilometragem
		check (quilometragem >= 0));	

create table grupo (
	codigo				smallserial		not null, 		
	nome				varchar(40)		not null,
	constraint pk_grupo
		primary key (codigo));

create table acessorio (
    sigla				varchar(10)		not null,
    nome				varchar(40)		not null,
    constraint pk_acessorio
    	primary key (sigla));
        
create table veiculo_acessorio (
    id_veiculo			integer			not null,
    sigla_acessorio		varchar(10)		not null,
    constraint pk_veiculo_acessorio
    	primary key (id_veiculo, sigla_acessorio));
    
-- Criação das chaves estrangeiras

alter table veiculo
	add constraint fk_veiculo_grupo
		foreign key (codigo_grupo)
		references grupo;

alter table veiculo_acessorio 
	add constraint fk_veiculo_acessorio_veiculo
    	foreign key (id_veiculo)
        references veiculo
        on delete cascade,
	add constraint fk_veiculo_acessorio_acessorio
    	foreign key (sigla_acessorio)
        references acessorio
        on update cascade;


-- 

delete 
from veiculo;

delete 
from grupo;

insert into grupo (nome)
values
('básico'),
('luxo');

insert into veiculo (placa, ano_fabricacao, data_aquisicao, data_venda, quilometragem, codigo_grupo)
values
('FGH4567', 2015, '2016-01-03', null, 123700, (select codigo from grupo where nome = 'básico')),
('XYZ5678', 2015, '2015-02-05', '2017-08-03', 456700, (select codigo from grupo where nome = 'luxo')),
('ABC1234', 2019, '2022-01-05', null, 200, (select codigo from grupo where nome = 'luxo'));

select *
from grupo;

select *
from veiculo;

update 	veiculo
	set ano_fabricacao = 2020,
    	data_aquisicao = '2022-01-07'
where	placa = 'ABC1234';        

select 'Claudio '||trim('Neiva        ')||'*',
		length (concat('Claudio ','Neiva')),
        position('Neiva' in 'Claudio Neiva') ;

select * 
from veiculo
where placa <> 'asd';

select * 
from veiculo
where data_venda is not null;

select	placa,
		data_aquisicao,
		coalesce(data_venda :: varchar, 'Veículo não vendido') as data_venda,
		coalesce(cast(data_venda as varchar), 'Veículo não vendido') as data_venda
from veiculo
where upper(placa) like 'ABC%';

select  ano_fabricacao
from	veiculo;

select  distinct ano_fabricacao
from	veiculo;

select CURRENT_DATE, CURRENT_TIME, CURRENT_TIMESTAMP;

select extract (year from CURRENT_DATE);
-- year, month, day, hour, minute...
select extract (hour from CURRENT_TIMESTAMP);
-- year, month, day, hour, minute...

select  count(*) as qtd_total_veiculos, 
        count(data_venda) as qtd_veiculos_vendidos,
		max(quilometragem) as maior_quilometragem_rodada_um_veiculo, 
        min(quilometragem) as menor_quilometragem_rodada_um_veiculo, 
        sum(quilometragem) as soma_quilometragens, 
        avg(quilometragem) as media_quilometragens
from veiculo;

select	ano_fabricacao,
		count(*)
from	veiculo
group by ano_fabricacao;   

-- Retorne os anos de fabricacao que tiveram 2 ou mais veículos cadastrados
select	ano_fabricacao
from	veiculo
group by ano_fabricacao
having 	count(*) >= 2;   

select	placa, ano_fabricacao, codigo_grupo
from veiculo
order by codigo_grupo desc,
 ano_fabricacao desc;




