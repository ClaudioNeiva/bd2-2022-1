package br.ucsal.bes20202.bd2.escola;

import javax.persistence.AttributeConverter;

//@Converter(autoApply = true)
public class SituacaoAlunoConverter implements AttributeConverter<SituacaoAlunoEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoAlunoEnum attribute) {
		return attribute == null ? null : attribute.getCodigo();
	}

	@Override
	public SituacaoAlunoEnum convertToEntityAttribute(String dbData) {
		return dbData == null ? null : SituacaoAlunoEnum.valueOfCodigo(dbData);
	}

}
