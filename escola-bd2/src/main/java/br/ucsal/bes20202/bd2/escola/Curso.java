package br.ucsal.bes20202.bd2.escola;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_curso")
public class Curso implements Serializable {

	private static final long serialVersionUID = 1L;

	// char(3)
	@Id
	@Column(columnDefinition = "char(3)")
	private String codigo;

	// varchar(40)
	@Column(length = 40)
	private String nome;

	// date
	private LocalDate dataCriacao;

	@ManyToMany(mappedBy = "cursos")
	private List<Aluno> alunos;

	public Curso() {
		super();
	}

	public Curso(String codigo, String nome, LocalDate dataCriacao) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.dataCriacao = dataCriacao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDate dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	@Override
	public int hashCode() {
		return Objects.hash(alunos, codigo, dataCriacao, nome);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Curso other = (Curso) obj;
		return Objects.equals(alunos, other.alunos) && Objects.equals(codigo, other.codigo)
				&& Objects.equals(dataCriacao, other.dataCriacao) && Objects.equals(nome, other.nome);
	}

	@Override
	public String toString() {
		return "Curso [codigo=" + codigo + ", nome=" + nome + ", dataCriacao=" + dataCriacao + "]";
	}

}
