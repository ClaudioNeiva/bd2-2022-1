package br.ucsal.bes20202.bd2.escola;

public enum SituacaoAlunoEnum {

	ATIVO("ATV"),

	FORMADO("FRM"),

	TRANCADO("TRC");

	private String codigo;

	private SituacaoAlunoEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoAlunoEnum valueOfCodigo(String codigo) {
		for (SituacaoAlunoEnum situacao : values()) {
			if (situacao.getCodigo().equalsIgnoreCase(codigo)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException("Código não encontrado: " + codigo);
	}

}
