package br.ucsal.bes20202.bd2.escola;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class Exemplo2 {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escola");
		EntityManager em = emf.createEntityManager();

		popularBase(em);

		// consultarAluno(em);

		// consultarAlunosPorSituacaoObject(em);

		// consultarAlunosPorSituacaoDto(em);

		// consultarUfs(em);

		// consultarAlunosNamedQuery(em);

		// consultarCidadesQueryNativa(em);
		
		exemplificarOrphanRemove(em);

		em.close();

		emf.close();
	}

	private static void exemplificarOrphanRemove(EntityManager em) {
		em.clear();
		Uf ufSP = em.find(Uf.class, "SP");
		System.out.println(ufSP.getCidades());

		em.getTransaction().begin();
		ufSP.getCidades().remove(2);
		em.getTransaction().commit();

		em.clear();
		ufSP = em.find(Uf.class, "SP");
		System.out.println(ufSP.getCidades());
	}

	private static void consultarCidadesQueryNativa(EntityManager em) {
		String sql = "select a.nome as nomeAluno, c.nome as NomeCidade from tab_aluno a inner join tab_cidade c on (a.cidade = c.sigla)";
		Query query = em.createNativeQuery(sql);
		List<Object[]> results = query.getResultList();
		results.forEach(r -> System.out.println("aluno=" + r[0] + ", cidade=" + r[1]));
	}

	private static void consultarAlunosNamedQuery(EntityManager em) {
		TypedQuery<Aluno> query = em.createNamedQuery("alunos da situação", Aluno.class);

		query.setParameter("situacao", SituacaoAlunoEnum.ATIVO);

		List<Aluno> alunos = query.getResultList();

		System.out.println("Alunos por ordem de nome:");

		alunos.stream().forEach(System.out::println);
	}

	private static void consultarUfs(EntityManager em) {
		// Forçar a carga das UFs e suas repectivas cidades.
		em.clear();

		String jpql = "select distinct u from Uf u left outer join fetch u.cidades";

		TypedQuery<Uf> query = em.createQuery(jpql, Uf.class);

		List<Uf> ufs = query.getResultList();

		System.out.println("**********************");

		em.close();

		ufs.forEach(u -> System.out.println(u.getSigla() + " - cidades=" + u.getCidades()));

	}

	private static void consultarAlunosPorSituacaoDto(EntityManager em) {
		String jpql = "select new " + ResultadoDTO.class.getCanonicalName() + "(a.situacao, count(*)) from Aluno a group by a.situacao";

		TypedQuery<ResultadoDTO> query = em.createQuery(jpql, ResultadoDTO.class);

		List<ResultadoDTO> resultado = query.getResultList();

		System.out.println("Quantidade de alunos por situação");

		System.out.println(resultado);
	}

	private static void consultarAlunosPorSituacaoObject(EntityManager em) {
		String jpql = "select a.situacao, count(*) from Aluno a group by a.situacao";

		TypedQuery<Object[]> query = em.createQuery(jpql, Object[].class);

		List<Object[]> resultado = query.getResultList();

		System.out.println("Quantidade de alunos por situação");

		resultado.forEach(r -> System.out.println(r[0] + " x " + r[1]));
	}

	private static void consultarAluno(EntityManager em) {

		String jpql = "select a from Aluno a where a.endereco.bairro = :bairro and TYPE(a) = Aluno order by a.nome asc";

		TypedQuery<Aluno> query = em.createQuery(jpql, Aluno.class);

		query.setParameter("bairro", "Pituaçu");

		List<Aluno> alunos = query.getResultList();

		System.out.println("Alunos que moram em Pituaçu:");

		alunos.stream().forEach(System.out::println);
	}

	private static void popularBase(EntityManager em) {
		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeCMP = new Cidade(ufSP, "CMP", "Campinas");
		em.persist(cidadeSAO);
		em.persist(cidadeGRU);
		em.persist(cidadeCMP);

		Curso cursoBES = new Curso("BES", "Bacharelado em Engenharia de Software", LocalDate.of(2014, 01, 05));

		Aluno alunoClaudio = new Aluno("Claudio Neiva", SituacaoAlunoEnum.FORMADO);
		alunoClaudio.setEndereco(new Endereco("Rua 1", "123", "Pituaçu", cidadeSAO));
		alunoClaudio.addCurso(cursoBES);
		em.persist(alunoClaudio);

		AlunoExterno alunoPedro = new AlunoExterno("Pedro", SituacaoAlunoEnum.ATIVO, "UFBA");
		alunoPedro.setEndereco(new Endereco("Rua 2", "456", "Pituaçu", cidadeGRU));
		alunoPedro.addCurso(cursoBES);
		em.persist(alunoPedro);

		AlunoExterno alunoJoaquim = new AlunoExterno("Joaquim", SituacaoAlunoEnum.ATIVO, "UFBA");
		alunoJoaquim.setEndereco(new Endereco("Rua 3", "867", "Brotas", cidadeSAO));
		alunoJoaquim.addCurso(cursoBES);
		em.persist(alunoJoaquim);

		em.getTransaction().commit();
	}

}
