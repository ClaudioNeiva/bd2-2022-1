package br.ucsal.bes20202.bd2.escola;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escola");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");
		// em.persist(ufSP);

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeSBC = new Cidade(ufSP, "SBC", "São Bernardo do Campo");
		em.persist(cidadeSAO);
		em.persist(cidadeGRU);
		em.persist(cidadeSBC);

		// Bidirecional?
		// Cascade?

		Curso cursoBES = new Curso("BES", "Bacharelado em Engenharia de Software", LocalDate.of(2014, 01, 05));
		// em.persist(cursoBES);

		Aluno alunoClaudio = new Aluno("Claudio Neiva", SituacaoAlunoEnum.FORMADO);
		alunoClaudio.setEndereco(new Endereco("Rua 1", "123", "Pituaçu", cidadeSAO));
		alunoClaudio.addCurso(cursoBES);
		em.persist(alunoClaudio);

		em.getTransaction().commit();

		em.getTransaction().begin();

//		em.detach(alunoClaudio);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(alunoClaudio);
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bais);
		alunoClaudio = (Aluno) ois.readObject();
		
		System.out.println(em.contains(alunoClaudio));
		System.out.println(em.contains(alunoClaudio.getCursos().get(0)));
		alunoClaudio.setNome("Maria");
		alunoClaudio.getCursos().get(0).setNome("Análise e Desenvolvimento de Sistemas");

		em.getTransaction().commit();

		em.close();

		emf.close();
	}

}
