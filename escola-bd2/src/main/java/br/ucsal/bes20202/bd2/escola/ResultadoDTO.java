package br.ucsal.bes20202.bd2.escola;

public class ResultadoDTO {

	private SituacaoAlunoEnum situacao;

	private Long quantidade;

	public ResultadoDTO(SituacaoAlunoEnum situacao, Long quantidade) {
		this.situacao = situacao;
		this.quantidade = quantidade;
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	@Override
	public String toString() {
		return "ResultadoDTO [situacao=" + situacao + ", quantidade=" + quantidade + "]";
	}

}
