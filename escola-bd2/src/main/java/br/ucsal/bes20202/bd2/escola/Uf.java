package br.ucsal.bes20202.bd2.escola;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_uf")
public class Uf implements Serializable {

	private static final long serialVersionUID = 1L;

	// char(2)
	@Id
	@Column(columnDefinition = "char(2)")
	private String sigla;

	// varchar(40)
	@Column(length = 40)
	private String nome;

	@OneToMany(mappedBy = "uf", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Cidade> cidades;

	public Uf() {
	}

	public Uf(String sigla, String nome) {
		super();
		this.sigla = sigla;
		this.nome = nome;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nome, sigla);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Uf other = (Uf) obj;
		return Objects.equals(nome, other.nome) && Objects.equals(sigla, other.sigla);
	}

	@Override
	public String toString() {
		return "Uf [sigla=" + sigla + ", nome=" + nome + "]";
	}

}
