package br.ucsal.bes20202.bd2.escola;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_aluno", uniqueConstraints = { @UniqueConstraint(name = "un_aluno_cpf", columnNames = { "cpf" }),
		@UniqueConstraint(name = "un_aluno_rg", columnNames = { "num_rg", "orgao_expedidor", "uf_orgao_expedidor" }) })
@SequenceGenerator(name = "seq_aluno_matricula", sequenceName = "sq_aluno_matricula")
@Inheritance(strategy = InheritanceType.JOINED)
//@DiscriminatorColumn(name = "tipo", length = 3, discriminatorType = DiscriminatorType.STRING)
//@DiscriminatorValue("ALN")
@NamedQuery(query = "select a from Aluno a where a.situacao = :situacao order by a.nome", 
name = "alunos da situação")
public class Aluno implements Serializable {

	private static final long serialVersionUID = 1L;

	// int
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_aluno_matricula")
	private Integer matricula;

	// varchar(40)
	@Column(length = 40, nullable = false)
	private String nome;

	// char(11)
	@Column(columnDefinition = "char(11)")
	private String cpf;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH })
	@JoinTable(name = "tab_aluno_curso", joinColumns = { @JoinColumn(name = "matricula_aluno") }, inverseJoinColumns = {
			@JoinColumn(name = "codigo_curso") }, foreignKey = @ForeignKey(name = "fk_aluno_curso_aluno"), inverseForeignKey = @ForeignKey(name = "fk_aluno_curso_curso"))
	private List<Curso> cursos;

	// char(3)
	// @Enumerated(EnumType.STRING)
	@Convert(converter = SituacaoAlunoConverter.class)
	@Column(columnDefinition = "char(3)")
	private SituacaoAlunoEnum situacao;

	// numeric(10,2) - renda_familiar
	@Column(name = "renda_familiar", precision = 10, scale = 2)
	private BigDecimal rendaFamiliar;

	@Column(name = "num_rg")
	private Long numRg;

	@Column(name = "orgao_expedidor")
	private String orgaoExpedidor;

	@ManyToOne
	@JoinColumn(name = "uf_orgao_expedidor")
	private Uf ufOrgaoExpedidor;

	// cada telefone deve ser varchar(15)
	@ElementCollection
	@Column(length = 15)
	private List<String> telefones;

	@Embedded
	private Endereco endereco;

	public Aluno() {
		this.cursos = new ArrayList<>();
		this.telefones = new ArrayList<>();
	}

	public Aluno(String nome, SituacaoAlunoEnum situacaoAlunoEnum) {
		this();
		this.nome = nome;
		this.situacao = situacaoAlunoEnum;
	}

	public Aluno(String nome, Curso curso, SituacaoAlunoEnum situacao) {
		this(nome, situacao);
		addCurso(curso);
	}

	public void addCurso(Curso curso) {
		this.cursos.add(curso);
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCurso(List<Curso> cursos) {
		this.cursos.clear();
		this.cursos.addAll(cursos);
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
	}

	public BigDecimal getRendaFamiliar() {
		return rendaFamiliar;
	}

	public void setRendaFamiliar(BigDecimal rendaFamiliar) {
		this.rendaFamiliar = rendaFamiliar;
	}

	public Long getNumRg() {
		return numRg;
	}

	public void setNumRg(Long numRg) {
		this.numRg = numRg;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public Uf getUfOrgaoExpedidor() {
		return ufOrgaoExpedidor;
	}

	public void setUfOrgaoExpedidor(Uf ufOrgaoExpedidor) {
		this.ufOrgaoExpedidor = ufOrgaoExpedidor;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones.clear();
		this.telefones.addAll(telefones);
	}

	public void addTelefone(String telefone) {
		telefones.add(telefone);
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cpf, cursos, endereco, matricula, nome, numRg, orgaoExpedidor, rendaFamiliar, situacao, telefones,
				ufOrgaoExpedidor);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		return Objects.equals(cpf, other.cpf) && Objects.equals(cursos, other.cursos) && Objects.equals(endereco, other.endereco)
				&& Objects.equals(matricula, other.matricula) && Objects.equals(nome, other.nome) && Objects.equals(numRg, other.numRg)
				&& Objects.equals(orgaoExpedidor, other.orgaoExpedidor) && Objects.equals(rendaFamiliar, other.rendaFamiliar)
				&& situacao == other.situacao && Objects.equals(telefones, other.telefones)
				&& Objects.equals(ufOrgaoExpedidor, other.ufOrgaoExpedidor);
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", cpf=" + cpf + ", situacao=" + situacao + ", rendaFamiliar="
				+ rendaFamiliar + ", numRg=" + numRg + ", orgaoExpedidor=" + orgaoExpedidor + ", ufOrgaoExpedidor=" + ufOrgaoExpedidor
				+ ", telefones=" + telefones + ", endereco=" + endereco + "]";
	}

}
