package br.ucsal.bes20202.bd2.escola;

import java.io.IOException;
import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo3 {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escola");
		EntityManager em = emf.createEntityManager();

		Integer matriculaAluno = carregarDados(em);

		em.clear();

		System.out.println("**********************************************1");

		Cidade cidade1 = em.find(Cidade.class, "SAO");

		Aluno aluno1 = em.find(Aluno.class, matriculaAluno);

		System.out.println("**********************************************2");

		System.out.println("cidade1=" + cidade1);

		System.out.println("aluno1=" + aluno1);
		
		System.out.println("aluno1.curso[0]=" + aluno1.getCursos().get(0));

		em.close();

		emf.close();
	}

	private static Integer carregarDados(EntityManager em) {
		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");
		// em.persist(ufSP);

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeSBC = new Cidade(ufSP, "SBC", "São Bernardo do Campo");
		em.persist(cidadeSAO);
		em.persist(cidadeGRU);
		em.persist(cidadeSBC);

		// Bidirecional?
		// Cascade?

		Curso cursoBES = new Curso("BES", "Bacharelado em Engenharia de Software", LocalDate.of(2014, 01, 05));
		// em.persist(cursoBES);

		Aluno alunoClaudio = new Aluno("Claudio Neiva", SituacaoAlunoEnum.FORMADO);
		alunoClaudio.setEndereco(new Endereco("Rua 1", "123", "Pituaçu", cidadeSAO));
		alunoClaudio.addCurso(cursoBES);
		alunoClaudio.addTelefone("714563456");
		alunoClaudio.addTelefone("732231341");
		alunoClaudio.addTelefone("719992234");
		alunoClaudio.addTelefone("754576777");
		em.persist(alunoClaudio);

		em.getTransaction().commit();

		return alunoClaudio.getMatricula();
	}

}
