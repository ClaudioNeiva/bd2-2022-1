-- Create view perdido em virtude de reinicialização da máquina

-- TRIGGER:

CREATE OR REPLACE FUNCTION f_trg_idu_movimento()
RETURNS TRIGGER
AS
$$
BEGIN
    IF TG_OP = 'DELETE' THEN
	    RAISE NOTICE 'OLD = data=%;id_item=%,qtd=%',OLD.data, OLD.id_item, OLD.qtd;
    	RETURN OLD;
    ELSE
	    RAISE NOTICE 'NEW = data=%;id_item=%,qtd=%',NEW.data, NEW.id_item, NEW.qtd;
		RETURN NEW;
    END IF;
END;
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER trg_idu_movimento
BEFORE INSERT OR DELETE OR UPDATE of data, id_item, qtd
ON movimento
FOR EACH ROW
EXECUTE PROCEDURE f_trg_idu_movimento();

insert into movimento(data, id_item, qtd, historico)
values 
('2022-03-18',(select id from item where nome = 'parafuso'),10,'Compra de parafusos.');

select 	*
from	movimento;

delete
from	movimento;

select 	*
from	movimento;

