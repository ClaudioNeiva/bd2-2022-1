package br.ucsal.bes20221.bd2.aulajdbc.persistence;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import br.ucsal.bes20221.bd2.aulajdbc.domain.Correntista;
import br.ucsal.bes20221.bd2.aulajdbc.util.DbUtil;

@TestInstance(Lifecycle.PER_CLASS)
public class CorrentistaDAOTest {

	@BeforeAll
	void setupClass() throws SQLException {
		String usuario = "postgres";
		String senha = "postgresql";
		DbUtil.connect(usuario, senha);
	}

	@AfterAll
	void teardownClass() throws SQLException {
		DbUtil.close();
	}

	@BeforeEach
	void setup() throws SQLException {
		CorrentistaDAO.deleteAll();
	}

	@Test
	void testarFindAll() throws SQLException {
		// FIXME Criar e persistir lista de correntistas.
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		// FIXME Verificar n�o mais nulidade e sim exatamente os correntistas
		// retornados.
		Assertions.assertNotNull(correntistas);
	}

	@Test
	void testarInsert() throws SQLException {
		Correntista correntista = new Correntista("Manuela", "123123", 2000);
		CorrentistaDAO.persist(correntista);
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		Assertions.assertAll(() -> Assertions.assertTrue(correntistas.contains(correntista)),
				() -> Assertions.assertNotNull(correntista.getId()));
	}

	@Test
	void testarUpate() throws SQLException {
		Correntista correntista = new Correntista("Joaquim", "123123", 2000);
		CorrentistaDAO.persist(correntista);
		correntista.setNome("Jo�o");
		correntista.setTelefone("98723");
		correntista.setAnoNascimento(2010);
		CorrentistaDAO.update(correntista);
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		Assertions.assertTrue(correntistas.contains(correntista));
	}

	@Test
	void testarDelete() throws SQLException {
		Correntista correntista = new Correntista("Joaquim", "123123", 2000);
		CorrentistaDAO.persist(correntista);
		CorrentistaDAO.delete(correntista);
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		Assertions.assertEquals(0, correntistas.size());
	}
}
