package br.ucsal.bes20221.bd2.aulajdbc.util;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20221.bd2.aulajdbc.util.DbUtil;

public class DbUtilTest {

	@Test
	void testarConnectarValido() throws SQLException {
		String usuario = "postgres";
		String senha = "postgresql";
		DbUtil.connect(usuario, senha);
		Connection connection = DbUtil.getConnection();
		Assertions.assertNotNull(connection);
		connection.close();
	}

	@Test
	void testarConnectarUsuarioInvalido() throws SQLException {
		String usuario = "postgresABC";
		String senha = "postgresql";
		Assertions.assertThrows(SQLException.class, () -> DbUtil.connect(usuario, senha));
	}

	@Test
	void testarObterConexaoNaoAberta() throws SQLException {
		Assertions.assertThrows(SQLException.class, () -> DbUtil.getConnection());
	}

}
