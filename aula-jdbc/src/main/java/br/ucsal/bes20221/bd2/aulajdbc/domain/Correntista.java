package br.ucsal.bes20221.bd2.aulajdbc.domain;

import java.util.Objects;

public class Correntista {

	private Integer id;
	private String nome;
	private String telefone;
	private Integer anoNascimento;

	public Correntista(String nome, String telefone, Integer anoNascimento) {
		this.nome = nome;
		this.telefone = telefone;
		this.anoNascimento = anoNascimento;
	}

	public Correntista(Integer id, String nome, String telefone, Integer anoNascimento) {
		this(nome, telefone, anoNascimento);
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public int hashCode() {
		return Objects.hash(anoNascimento, nome, telefone);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Correntista other = (Correntista) obj;
		return Objects.equals(anoNascimento, other.anoNascimento) && Objects.equals(nome, other.nome)
				&& Objects.equals(telefone, other.telefone);
	}

	@Override
	public String toString() {
		return "Correntista [id=" + id + ", nome=" + nome + ", telefone=" + telefone + ", anoNascimento="
				+ anoNascimento + "]\n";
	}
}
