package br.ucsal.bes20221.bd2.aulajdbc.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20221.bd2.aulajdbc.domain.Correntista;
import br.ucsal.bes20221.bd2.aulajdbc.util.DbUtil;

public class CorrentistaDAO {

	private static final String QUERY_FIND_ALL = "select id, nm, nu_telefone, nu_ano_nascimento from correntista order by nm asc";

	private static final String QUERY_INSERT = "insert into correntista (nm, nu_telefone, nu_ano_nascimento) values (?, ?, ?)";

	private static final String QUERY_UPDATE = "update correntista set nm=?, nu_telefone=?, nu_ano_nascimento=? where id=?";

	private static final String QUERY_DELETE = "delete from correntista where id=?";

	private static final String QUERY_DELETE_ALL = "delete from correntista";

	private CorrentistaDAO() {
	}

	public static List<Correntista> findAll() throws SQLException {
		List<Correntista> correntistas = new ArrayList<>();

		try (Statement stmt = DbUtil.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE); ResultSet rs = stmt.executeQuery(QUERY_FIND_ALL)) {
			while (rs.next()) {
				correntistas.add(resultSet2Correntista(rs));
			}
		}
		return correntistas;
	}

	public static void persist(Correntista correntista) throws SQLException {
		try (PreparedStatement stmt = DbUtil.getConnection().prepareStatement(QUERY_INSERT,
				Statement.RETURN_GENERATED_KEYS)) {
			stmt.setString(1, correntista.getNome());
			stmt.setString(2, correntista.getTelefone());
			stmt.setInt(3, correntista.getAnoNascimento());
			stmt.executeUpdate();
			correntista.setId(getGeneratedId(stmt));
		}
	}

	public static void update(Correntista correntista) throws SQLException {
		try (PreparedStatement stmt = DbUtil.getConnection().prepareStatement(QUERY_UPDATE)) {
			stmt.setString(1, correntista.getNome());
			stmt.setString(2, correntista.getTelefone());
			stmt.setInt(3, correntista.getAnoNascimento());
			stmt.setInt(4, correntista.getId());
			stmt.executeUpdate();
		}
	}

	public static void deleteAll() throws SQLException {
		try (Statement stmt = DbUtil.getConnection().createStatement()) {
			stmt.executeUpdate(QUERY_DELETE_ALL);
		}
	}

	public static void delete(Correntista correntista) throws SQLException {
		delete(correntista.getId());
	}

	public static void delete(Integer id) throws SQLException {
		try (PreparedStatement stmt = DbUtil.getConnection().prepareStatement(QUERY_DELETE)) {
			stmt.setInt(1, id);
			stmt.executeUpdate();
		}
	}

	private static Integer getGeneratedId(Statement stmt) throws SQLException {
		try (ResultSet idRs = stmt.getGeneratedKeys()) {
			idRs.next();
			return idRs.getInt(1);
		}
	}

	private static Correntista resultSet2Correntista(ResultSet rs) throws SQLException {
		Integer id = rs.getInt("id");
		String nome = rs.getString("nm");
		String telefone = rs.getString("nu_telefone");
		Integer anoNascimento = rs.getInt("nu_ano_nascimento");
		return new Correntista(id, nome, telefone, anoNascimento);
	}

}
