package br.ucsal.bes20221.bd2.aulajdbc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {

	private static final String URL = "jdbc:postgresql://localhost:5432/aula-jdbc";

	private static Connection connection;

	private DbUtil() {
	}

	public static void connect(String user, String password) throws SQLException {
		connection = DriverManager.getConnection(URL, user, password);
	}

	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			throw new SQLException("Conex�o n�o dispon�vel.");
		}
		return connection;
	}

	public static void close() throws SQLException {
		if (connection != null) {
			connection.close();
		}
	}
}

