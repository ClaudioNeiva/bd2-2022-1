CREATE OR REPLACE FUNCTION f_trg_idu_movimento()
RETURNS TRIGGER
AS
$$
BEGIN
    IF TG_OP IN ('INSERT', 'UPDATE') THEN
		EXECUTE f_atualizar_saldo_ins(NEW);
	END IF;
    IF TG_OP IN ('DELETE', 'UPDATE' ) THEN
		EXECUTE f_atualizar_saldo_del(OLD);
	END IF;

	IF TG_OP = 'DELETE' THEN
    	RETURN OLD;
    ELSE
		RETURN NEW;
    END IF;
END;
$$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION f_atualizar_saldo_ins(p_movimento movimento)
RETURNS VOID
AS
$$
DECLARE
	v_data_saldo_anterior	date;
    v_qtd_saldo_anterior	int;
BEGIN
	-- obter a data do saldo anterior
    SELECT 	MAX(s.data)
    INTO 	v_data_saldo_anterior
    FROM 	saldo s
    WHERE	s.id_item = p_movimento.id_item
    AND		s.data <= p_movimento.data;
    
    -- se existe data do saldo anterior, obter o valor do saldo nesse data
    IF v_data_saldo_anterior is not null THEN
        SELECT 	s.qtd
        INTO 	v_qtd_saldo_anterior
        FROM 	saldo s
        WHERE	s.id_item = p_movimento.id_item
        AND		s.data = v_data_saldo_anterior;
	ELSE
    	v_qtd_saldo_anterior = 0;
    END IF;
    
    -- se a data do saldo anterior for diferente da data do movimento, inserir um saldo nessa data
    IF v_data_saldo_anterior is null or v_data_saldo_anterior <> p_movimento.data THEN
        INSERT INTO saldo (data, id_item, qtd)
        VALUES (p_movimento.data, p_movimento.id_item, v_qtd_saldo_anterior);
    END IF;
    
    -- atualizar os saldos >=  a data do movimento com a quantidade do movimento
    UPDATE 	saldo
    SET		qtd = qtd + p_movimento.qtd
    WHERE	id_item = p_movimento.id_item
    AND		data >= p_movimento.data;
END;
$$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION f_atualizar_saldo_del(p_movimento movimento)
RETURNS VOID
AS
$$
BEGIN
    UPDATE 	saldo
    SET		qtd = qtd - p_movimento.qtd
    WHERE	id_item = p_movimento.id_item
    AND		data >= p_movimento.data;
END;
$$
LANGUAGE PLPGSQL;

DROP TRIGGER IF EXISTS trg_idu_movimento ON movimento;

CREATE TRIGGER trg_idu_movimento
BEFORE INSERT OR DELETE OR UPDATE OF data, id_item, qtd
ON movimento
FOR EACH ROW
EXECUTE PROCEDURE f_trg_idu_movimento();

