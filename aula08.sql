CREATE OR REPLACE FUNCTION f_trg_idu_movimento()
RETURNS TRIGGER
AS
$$
BEGIN
    IF TG_OP IN ('INSERT', 'UPDATE') THEN
		EXECUTE f_atualizar_saldo_ins(NEW);
	END IF;
    IF TG_OP IN ('DELETE', 'UPDATE' ) THEN
		EXECUTE f_atualizar_saldo_del(OLD);
	END IF;

	IF TG_OP = 'DELETE' THEN
    	RETURN OLD;
    ELSE
		RETURN NEW;
    END IF;
END;
$$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION f_atualizar_saldo_ins(p_movimento movimento)
RETURNS VOID
AS
$$
DECLARE 
	v_saldo_anterior		t_saldo;
BEGIN
	v_saldo_anterior = f_obter_saldo_anterior(p_movimento.id_item , p_movimento.data);
	
    -- se a data do saldo anterior for diferente da data do movimento, inserir um saldo nessa data
    IF v_saldo_anterior.data is null or v_saldo_anterior.data <> p_movimento.data THEN
        INSERT INTO saldo (data, id_item, qtd)
        VALUES (p_movimento.data, p_movimento.id_item, v_saldo_anterior.qtd);
    END IF;
    
    -- atualizar os saldos >=  a data do movimento com a quantidade do movimento
    UPDATE 	saldo
    SET		qtd = qtd + p_movimento.qtd
    WHERE	id_item = p_movimento.id_item
    AND		data >= p_movimento.data;
END;
$$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION f_atualizar_saldo_del(p_movimento movimento)
RETURNS VOID
AS
$$
BEGIN
    UPDATE 	saldo
    SET		qtd = qtd - p_movimento.qtd
    WHERE	id_item = p_movimento.id_item
    AND		data >= p_movimento.data;
END;
$$
LANGUAGE PLPGSQL;

create or replace function f_obter_data_saldo_anterior(p_id_item int , p_data date)
returns date
as
$$
declare 
	v_data_saldo_anterior	date;
begin
     SELECT 	MAX(s.data)
        INTO 	v_data_saldo_anterior
        FROM 	saldo s
        WHERE	s.id_item = p_id_item
        AND		s.data <= p_data;
    return v_data_saldo_anterior;
end;
$$ language plpgsql;

create or replace function f_obter_qtd_saldo_anterior(p_id_item int , p_data date)
returns int
as
$$
declare 
	v_qtd_saldo_anterior	int;
begin
    IF p_data is not null THEN
        SELECT 	s.qtd
        INTO 	v_qtd_saldo_anterior
        FROM 	saldo s
        WHERE	s.id_item = p_id_item
        AND		s.data = p_data;
	ELSE
    	v_qtd_saldo_anterior = 0;
    END IF;
    return v_qtd_saldo_anterior;
end;
$$ language plpgsql;

create or replace function f_obter_saldo_anterior_v1(p_id_item int , p_data date, OUT p_data_saldo date, OUT p_qtd_saldo int)
returns record
as
$$
begin
	p_data_saldo = f_obter_data_saldo_anterior(p_id_item, p_data);
	p_qtd_saldo = f_obter_qtd_saldo_anterior(p_id_item, p_data_saldo);
end;
$$ language plpgsql;

create or replace function f_obter_saldo_anterior_v2(p_id_item int , p_data date)
returns saldo
as
$$
declare
	v_saldo		saldo;
begin
	v_saldo.data = f_obter_data_saldo_anterior(p_id_item, p_data);
	v_saldo.qtd = f_obter_qtd_saldo_anterior(p_id_item, v_saldo.data);
    return v_saldo;
end;
$$ language plpgsql;

create type t_saldo as (data date, qtd int);

create or replace function f_obter_saldo_anterior(p_id_item int , p_data date)
returns t_saldo
as
$$
declare
	v_saldo		t_saldo;
begin
	v_saldo.data = f_obter_data_saldo_anterior(p_id_item, p_data);
	v_saldo.qtd = f_obter_qtd_saldo_anterior(p_id_item, v_saldo.data);
    return v_saldo;
end;
$$ language plpgsql;

do
$$
declare 
	v_saldo2	saldo;
	v_data1		date;
    v_qtd1 		int;
	v_saldo		t_saldo;
begin

	select p_data_saldo, p_qtd_saldo into v_data1, v_qtd1 from f_obter_saldo_anterior_v1(118 , '2022-03-14');
    raise notice 'data=%',v_data1;
    raise notice 'qtd=%',v_qtd1;
    
	v_saldo2 = f_obter_saldo_anterior_v2(118 , '2022-03-14');
    raise notice 'data=%',v_saldo2.data;
    raise notice 'qtd=%',v_saldo2.qtd;

	v_saldo = f_obter_saldo_anterior(118 , '2022-03-14');
    raise notice 'data=%',v_saldo.data;
    raise notice 'qtd=%',v_saldo.qtd;
end;
$$

select * 
from saldo;

DROP TRIGGER IF EXISTS trg_idu_movimento ON movimento;

CREATE TRIGGER trg_idu_movimento
BEFORE INSERT OR DELETE OR UPDATE OF data, id_item, qtd
ON movimento
FOR EACH ROW
EXECUTE PROCEDURE f_trg_idu_movimento();
