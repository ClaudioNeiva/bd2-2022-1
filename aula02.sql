-- Criação das tabelas

create table veiculo (
	id					serial			not null,		
	placa				char(7)			not null,    	
	ano_fabricacao		integer			not null, 
	data_aquisicao		date 			not null, 
	data_venda			date			    null, 	
	quilometragem		integer			not null, 
	codigo_grupo		smallint		not null,		
	constraint pk_veiculo
		primary key (id),
	constraint un_veiculo_placa
		unique (placa),
	constraint ck_veiculo_quilometragem
		check (quilometragem >= 0));	

create table grupo (
	codigo				smallserial		not null, 		
	nome				varchar(40)		not null,
	constraint pk_grupo
		primary key (codigo));
	
-- Criação das chaves estrangeiras

alter table veiculo
	add constraint fk_veiculo_grupo
		foreign key (codigo_grupo)
		references grupo;
